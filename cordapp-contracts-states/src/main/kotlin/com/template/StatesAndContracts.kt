package com.template

import net.corda.core.contracts.*
import net.corda.core.contracts.Requirements.using
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.transactions.LedgerTransaction

val PRODUCT_CONTRACT_ID = "com.template.ProductContract"

open class ProductContract : Contract {
    override fun verify(tx: LedgerTransaction) {
        val command = tx.commands.requireSingleCommand<ProductContract.Commands>()
        when (command.value) {
            is ProductContract.Commands.Create -> {
                "Must have no input state." using (tx.inputStates.isEmpty())
                "Must have output state." using (tx.outputStates.isNotEmpty())
            }
            is ProductContract.Commands.Sell -> {
                val input = tx.inputStates.filterIsInstance<ProductState>().single()
                requireThat {
                    "The transaction is signed by the owner of the state" using (input.owner.owningKey in command.signers)
                    "Must have output state" using (tx.outputStates.filterIsInstance<ProductState>().size == 1)
                }
            }
            is ProductContract.Commands.IncreaseSellPrice -> {
                "Must have input state." using (tx.inputStates.isNotEmpty())
                "Must have output state." using (tx.outputStates.isNotEmpty())
                val input = tx.inputStates.filterIsInstance<ProductState>().single()
                val output = tx.outputStates.filterIsInstance<ProductState>().single()
                "Sell Price must be greater than earlier." using (output.price > input.price)
                "The transaction is signed by the owner of the state" using (input.owner.owningKey in command.signers)
            }
            else -> throw IllegalArgumentException("Unrecognised command")
        }
    }

    // Used to indicate the transaction's intent.
    interface Commands : CommandData {
        class Create : Commands, TypeOnlyCommandData()
        class IncreaseSellPrice : Commands, TypeOnlyCommandData()
        class Sell : Commands, TypeOnlyCommandData()
    }
}

data class ProductState(val name: String,
                        val color: String,
                        val price: Double,
                        val seller: Party,
                        override val owner: AbstractParty) : OwnableState {
    override val participants = listOf(owner)

    override fun withNewOwner(newOwner: AbstractParty): CommandAndState {
        return CommandAndState(ProductContract.Commands.Sell(), this.copy(owner = newOwner))
    }
}