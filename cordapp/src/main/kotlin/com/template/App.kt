package com.template

import com.template.flow.CreateProductFlow
import com.template.flow.IncreaseSellPriceFlow
import com.template.flow.SellProductFlow
import net.corda.core.messaging.CordaRPCOps
import net.corda.core.node.services.Vault
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.serialization.SerializationWhitelist
import net.corda.core.utilities.getOrThrow
import net.corda.webserver.services.WebServerPluginRegistry
import java.math.BigDecimal
import java.util.function.Function
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

// *****************
// * API Endpoints *
// *****************
@Path("trade")
class TemplateApi(val rpcOps: CordaRPCOps) {
    // Accessible at /api/template/templateGetEndpoint.
    @GET
    @Path("templateGetEndpoint")
    @Produces(MediaType.APPLICATION_JSON)
    fun templateGetEndpoint(): Response {
        return Response.ok("Template GET endpoint.").build()
    }

    @GET
    @Path("/products/create")
    @Produces(MediaType.APPLICATION_JSON)
    fun procure() {
        val me = rpcOps.nodeInfo().legalIdentities.first()

        val outState = ProductState("I Phone", "White", 50000.00, me, me)
        val flowHandle = rpcOps.startFlowDynamic(CreateProductFlow.Initiator::class.java, outState)
        flowHandle.returnValue.getOrThrow()
    }

    @GET
    @Path("products")
    @Produces(MediaType.APPLICATION_JSON)
    fun products(): Map<String, List<ProductState>> {
        val map = mutableMapOf<String, List<ProductState>>()

        val criteria = QueryCriteria.VaultQueryCriteria(status = Vault.StateStatus.UNCONSUMED)
        val states = rpcOps.vaultQueryByCriteria(criteria, ProductState::class.java).states.map { it.state.data }
        map.put("UNCONSUMED", states)

        val criteria2 = QueryCriteria.VaultQueryCriteria(status = Vault.StateStatus.CONSUMED)
        val states2 = rpcOps.vaultQueryByCriteria(criteria2, ProductState::class.java).states.map { it.state.data }
        map.put("CONSUMED", states2)
        return map
    }

    @GET
    @Path("products/increaseSellPrice/{sellPrice}")
    @Produces(MediaType.APPLICATION_JSON)
    fun increaseSellPrice(@PathParam("sellPrice") sellPrice: Double) {
        val flowHandle = rpcOps.startFlowDynamic(IncreaseSellPriceFlow.Initiator::class.java, sellPrice)
        flowHandle.returnValue.getOrThrow()
    }

    @GET
    @Path("sell/{partyName}")
    @Produces(MediaType.APPLICATION_JSON)
    fun sell(@PathParam("partyName") partyName: String) {
        val buyerPartyC = rpcOps.partiesFromName(partyName, true).single()

        val flowHandle = rpcOps.startFlowDynamic(SellProductFlow.InitiatorSeller::class.java, buyerPartyC)
        flowHandle.returnValue.getOrThrow()
    }


    @GET
    @Path("tx/states")
    @Produces(MediaType.APPLICATION_JSON)
    fun txStates() {
        val states: MutableList<ProductState> = mutableListOf()
        rpcOps.internalVerifiedTransactionsSnapshot().forEach {

        }
    }

}


// ***********
// * Plugins *
// ***********
class TemplateWebPlugin : WebServerPluginRegistry {
    // A list of classes that expose web JAX-RS REST APIs.
    override val webApis: List<Function<CordaRPCOps, out Any>> = listOf(Function(::TemplateApi))
    //A list of directories in the resources directory that will be served by Jetty under /web.
    // This template's web frontend is accessible at /web/template.
    override val staticServeDirs: Map<String, String> = mapOf(
            // This will serve the templateWeb directory in resources to /web/template
            "template" to javaClass.classLoader.getResource("templateWeb").toExternalForm()
    )
}

// Serialization whitelist.
class TemplateSerializationWhitelist : SerializationWhitelist {
    override val whitelist: List<Class<*>> = listOf(TemplateData::class.java)
}

// This class is not annotated with @CordaSerializable, so it must be added to the serialization whitelist, above, if
// we want to send it to other nodes within a flow.
data class TemplateData(val payload: String)
