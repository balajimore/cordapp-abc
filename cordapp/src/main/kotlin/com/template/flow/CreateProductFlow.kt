package com.template.flow

import co.paralleluniverse.fibers.Suspendable
import com.template.PRODUCT_CONTRACT_ID
import com.template.ProductContract
import com.template.ProductState
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndContract
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.InitiatingFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.transactions.TransactionBuilder


object CreateProductFlow {
    @InitiatingFlow
    @StartableByRPC
    class Initiator(val outState: ProductState) : FlowLogic<Unit>() {
        @Suspendable
        override fun call() {
            val notary = serviceHub.networkMapCache.notaryIdentities.first()
            val txb = TransactionBuilder(notary)
            val procureCmd = Command(ProductContract.Commands.Create(), outState.seller.owningKey)

            txb.withItems(StateAndContract(outState, PRODUCT_CONTRACT_ID))
            txb.withItems(procureCmd)

            txb.verify(serviceHub)
            val stx = serviceHub.signInitialTransaction(txb)
            subFlow(FinalityFlow(stx))
        }
    }
}