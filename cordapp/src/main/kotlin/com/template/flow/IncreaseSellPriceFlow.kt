package com.template.flow

import co.paralleluniverse.fibers.Suspendable
import com.template.PRODUCT_CONTRACT_ID
import com.template.ProductContract
import com.template.ProductState
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndContract
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

object IncreaseSellPriceFlow {
    @InitiatingFlow
    @StartableByRPC
    class Initiator(val newSellPrice: Double) : FlowLogic<Unit>() {
        @Suspendable
        override fun call() {
            val notary = serviceHub.networkMapCache.notaryIdentities.first()
            val txb = TransactionBuilder(notary)

            val inState = serviceHub.vaultService.queryBy(ProductState::class.java).states.first()
            val outState = inState.state.data.copy(price = newSellPrice)

            val procureCmd = Command(ProductContract.Commands.IncreaseSellPrice(), outState.owner.owningKey)
            txb.withItems(StateAndContract(outState, PRODUCT_CONTRACT_ID))
            txb.withItems(procureCmd)
            txb.withItems(inState)

            txb.verify(serviceHub)
            val stx = serviceHub.signInitialTransaction(txb)
            subFlow(FinalityFlow(stx))
        }
    }
}