package com.template.flow

import co.paralleluniverse.fibers.Suspendable
import com.template.PRODUCT_CONTRACT_ID
import com.template.ProductContract
import com.template.ProductState
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndContract
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

object SellProductFlow {
    @InitiatingFlow
    @StartableByRPC
    class InitiatorSeller(val buyer: Party) : FlowLogic<Unit>() {
        @Suspendable
        override fun call() {
            val notary = serviceHub.networkMapCache.notaryIdentities.first()
            val txb = TransactionBuilder(notary)

            val inState = serviceHub.vaultService.queryBy(ProductState::class.java).states.first()
            val outState = inState.state.data.copy(owner = buyer)

            val sellCmd = Command(ProductContract.Commands.Sell(), listOf(ourIdentity.owningKey, buyer.owningKey))
            txb.withItems(StateAndContract(outState, PRODUCT_CONTRACT_ID))
            txb.withItems(sellCmd)
            txb.withItems(inState)

            txb.verify(serviceHub)
            val ptx = serviceHub.signInitialTransaction(txb)

            val buyerSession = initiateFlow(buyer)
            val stx = subFlow(CollectSignaturesFlow(ptx, listOf(buyerSession)))

            subFlow(FinalityFlow(stx))
        }
    }

    @InitiatedBy(InitiatorSeller::class)
    class ResponderBuyer(val counterpartySession: FlowSession) : FlowLogic<Unit>() {
        @Suspendable
        override fun call() {
            val signTransactionFlow = object : SignTransactionFlow(counterpartySession) {
                override fun checkTransaction(stx: SignedTransaction) = requireThat {
                    "Must be signed by the initiator" using (stx.sigs.any())
                    val output = stx.tx.outputStates.filterIsInstance<ProductState>().single()
                    "Must have atleast one output state" using (output.owner == ourIdentity)
                }
            }
            subFlow(signTransactionFlow)
        }
    }
}